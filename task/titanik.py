import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.split(', ').str[1].str.split('.').str[0].str.strip()
    missed_values = [df[df['Title'] == 'Mr']['Age'].isnull().sum(), df[df['Title'] == 'Mrs']['Age'].isnull().sum(), df[df['Title'] == 'Miss']['Age'].isnull().sum()]
    median_values = df.groupby('Title')['Age'].median().round().astype(int)
    return [('Mr.', missed_values[0], median_values['Mr']), ('Mrs.', missed_values[1], median_values['Mrs']), ('Miss.', missed_values[2], median_values['Miss'])]